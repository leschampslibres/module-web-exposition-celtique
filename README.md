# Module web de l'exposition "Celtique ?"
Pilotée par le Musée de Bretagne aux Champs Libres

Ce site web a été réalisé sous [Eleventy](https://www.11ty.dev/), générateur de sites web statiques.

Il est accessible à cette adresse ▶ https://exposition-celtique.bzh

# LICENCES & DROITS D'USAGES

Dans une logique vertueuse d'ouverture & de libération des sources, l'ensemble des éléments du module sont placés sous licence libre (GNU General Public Licence) et partagés ici même, à partir du profil GitLab des Champs Libres. Vous pouvez le télécharger librement & en intégralité ([ZIP](https://gitlab.com/leschampslibres/module-web-exposition-celtique/-/archive/main/module-web-exposition-celtique-main.zip) | [TAR](https://gitlab.com/leschampslibres/module-web-exposition-celtique/-/archive/main/module-web-exposition-celtique-main.tar) | [TAR.GZ](https://gitlab.com/leschampslibres/module-web-exposition-celtique/-/archive/main/module-web-exposition-celtique-main.tar.gz) | [TAR.BZ2](https://gitlab.com/leschampslibres/module-web-exposition-celtique/-/archive/main/module-web-exposition-celtique-main.tar.bz2)), et disposez ainsi des droits inhérents à cette licence.

Merci de mentionner "Les Champs Libres" pour toute utilisation.

---

# MENTIONS LÉGALES

Date de création : mai 2022
Directrice de la publication : Corinne Poulain
Equipe projet :

- Conception & pilotage du projet, intégration des contenus : Manuel Moreau, Sarah Lemiale
- Conseil & accompagnement : Guillaume Rouan
- Réalisation graphique : [Fairness](https://fairness.coop)
- Réalisation technique : Nicolas Doby ([It's On Us](https://itsonus.fr))
- Support technique : DSI Rennes Métropole & Matthieu Delsaut

[![Les Champs Libres contribuent à Software Heritage](software-heritage-icon_365x80px.png)](https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.com/leschampslibres/module-web-exposition-celtique)

💬 N'hésitez pas à nous faire part de vos remarques, elles nous sont très précieuses pour améliorer ce module !

Les Champs Libres<br />
10, cours des Alliés - 35000 RENNES<br />
[@LesChampsLibres](https://twitter.com/LesChampsLibres)<br />
communication [arobase] leschampslibres [point] fr

---

![Licence GNU GPL v.3](gnugplv3-icon_136x68.png)

Les Champs Libres | LesChampsLibres.fr | Rennes
