# Installation du module web

Ce site web statique fonctionne sous [Elf starter kit](https://github.com/stowball/elf).

_Elf_ est un kit de démarrage [Eleventy](https://www.11ty.dev/) simplifié permettant de créer des projets web à l'aide de technologies standards telles que Webpack, Babel and Sass, tout en tenant compte de la facilité d'utilisation, des performances et de la compatibilité du navigateur.

Ce projet a besoin de [NodeJs](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) pour fonctionner.

## Pour bien commencer

En lignes de commande :

1. Cloner ou bifurquer (_fork_) ce dépôt : `git clone https://gitlab.com/leschampslibres/module-web-exposition-celtique.git`
2. `cd` depuis le répertoire du projet, puis lancer la commande `npm install`

## Exécuter & diffuser une version de développement

```
$ sh
$ npm run dev
```

Lancer un navigateur web et se rendre à l'adresse [http://localhost:8080](http://localhost:8080) pour visualiser le site.

## Créer une version de production

```
$ sh
$ npm run prod
```

### Servir la version de production avec un serveur Web externe

Si la construction est servie par un serveur web comme _Nginx_ ou _Tomcat_, copier simplement le répertoire `dist` de la construction dans le répertoire utilisé par le serveur web.

Ajouter une optimisation :
- compresser les requêtes avec gzip,
- configurer le cache sur les en-têtes de la requête,
- utiliser HTTP2.

Voir [cet exemple](https://homebrewserver.club/low-tech-website-howto.html#configuring-the-webserver) pour plus de détails.

### Servir la version de production avec _NodeJs_

Exécuter simplement en utilisant `npm` :

```
$ sh
$ npm run serve:prod
```

puis accéder à [http://localhost:3000](http://localhost:3000) pour visualiser.

## Technologies utilisées

* [Eleventy](https://www.11ty.dev/)
* [EJS](https://njk.co/) comme langage pour les templates
* [Sass](https://sass-lang.com/) pour l'écriture des CSS
* [Babel](https://babeljs.io/) pour compiler _JavaScript_
* [Autoprefixer](https://github.com/postcss/autoprefixer) pour gérer les préfixes CSS
* [Webpack](https://webpack.js.org/) pour compiler les actifs _Sass_ et _Javascript_
* [ESLint](https://eslint.org/) et [la configuration de base d'Airbnb](https://www.npmjs.com/package/eslint-config-airbnb-base) pour "épauler" l'écriture du code

## Structure du projet

```
src/
  _components/
    Tous les composants de l'interface utilisateur
  _data/
    Fichiers de données Eleventy
  _layouts/
    Mise en page de base
  _pages/
    Chaque modèle de page individuel
  assets/
    css/
      index.scss
      Tous les autres fichiers scss
    js/
      index.js
      Tous les autres fichiers js
  audios/
    Tous les fichiers audio utilisés (vide dans notre archive)
  images/
    Tous les fichiers images utilisés
  videos/
    Tous les fichiers vidéos utilisés (vide dans notre archive)
Fichiers de configuration et de compilation
```

Les fichiers dans `assets` seront gérés par _Webpack_, _Eleventy_ transformera tous les répertoires avec un `_` de tête et copiera tous les `audios`, `images` et `videos`.

La sortie d'_Eleventy_ sera dans un répertoire `dist` au niveau racine.
