module.exports = {
  url: process.env.ELEVENTY_ENV === 'production' ? 'www.exposition-celtique.bzh' : 'http://localhost:8080',
};
