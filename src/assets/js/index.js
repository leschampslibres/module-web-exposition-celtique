import '../css/index.scss';

// let lastScrollTop; // This Varibale will store the top position
const header = document.getElementById('header');

window.addEventListener('scroll', () => {
  if (window.screen.width >= 1024) {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    if (scrollTop > 50) {
      header.style.transform = 'translateY(-79px)';
    } else {
      header.style.transform = 'translateY(0)';
    }
  } else {
    header.style.transform = 'translateY(0)';
  }
});
